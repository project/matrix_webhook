### About

Get media provided by a matrix bot in a group

With this module one can get media from a matrix own bot to any drupal site. Once installed and bot is created through matrix-webhooker bot(https://gitlab.com/communia/matrix-webhooker) :

### Creating bot in matrix
0.  First install this module.
1.  Start a chat (group or direct) with the bot that is offering the webhooker functionality, (our crafted botfather) and create a command writing:

    `!webhooker command:set !c https://webhook.url/matrix_webhook/urltoken {"context": "in json"}`
    (You can use the the random url token that is automatically created if module is not configured in drupal side at /admin/config/services/matrix_webhook/hooks).

2.  `bot in DM> Succesful set command, summary:` Will provide the whole command properties in a table. And the webhooker token.
```
{
   "room_id": "!koewkopawkopvap:homeserver.net",
   "custom_command": "!c",
   "url": "https://webhook.url/matrix_webhook/urltoken",
   "token": "LONGTOKENxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
}
```
3. This webhook token must pe provided in first field at /admin/config/services/matrix_webhook/hooks . Also remember that `urltoken` fragment in 'url' property and the `Url token` in /admin/config/services/matrix_webhook/hooks must be the same.

3.  The matrix part is done, you can start collecting things.

### Setting the module

In Drupal part first of all, setting the hook must be done, in [hooks settings](/admin/config/services/matrix_webhook/hooks) it can be set to get user synced between telegram and site, a text field must be added to user entity. This field will be used to set the user id of saved media. If field doesn't exist a fallback user will be choosen, and if no fallback user is defined the user with uid 1 will be used. In [field mappings](/admin/config/services/matrix_webhook/hooks/field_mapping) the matrix username field and fallback user can be set. In the same [field mappings](/admin/config/services/matrix_webhook/hooks/field_mapping) settings the relation between obtained media and drupal content type can be set.

### Uses

To use this media collection tool from matrix(groups or directly) to any Drupal site, from a chat where bot is present launch:

`!c absolute_url description #tag #tag2` where description and tags are optionals.
Also it can be used replying to any link sent, with just:
`!c description #tag #tag1`
Again, description and tags are optional but recommended.

A new content will be created in site using the content type and the fields as specified

