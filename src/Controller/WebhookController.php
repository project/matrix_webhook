<?php

namespace Drupal\matrix_webhook\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\matrix_webhook\Collector;
use \Drupal\Core\Entity\Query\QueryFactory;
use \Drupal\Component\Utility\Unicode;
use \Drupal\node\NodeInterface;
use \Drupal\taxonomy\Entity\Term;

/**
 * Class WebhookController.
 */
class WebhookController extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;
  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\matrix_webhook\Collector Text elements collector
   *
   * @var \Drupal\matrix_webhook\Collector
   */
  protected $collector;

  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * Constructs a new WebhookController object.
   */
  public function __construct(ConfigFactory $config_factory, EntityTypeManager $entity_type_manager, Collector $collector, QueryFactory $entity_query) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->collector = $collector;
    $this->entityQuery = $entity_query;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('matrix_webhook.collector'),
      $container->get('entity.query')
    );
  }

  /**
   * Token.
   *
   * @return string
   *   Return Hello string.
   */
  public function token($token, Request $request) {
    $hook_settings = $this->configFactory->get('matrix_webhook.hooks_settings');

    $authorizationHeader = $request->headers->get('Authorization');
    $jwtToken = substr($authorizationHeader, 7);
    if ( $hook_settings->get('webhooker_token') != $jwtToken ){
      return new JsonResponse([
        'text' => 'error',
        'method' => 'GET',
      ], 401);
    }

    $post_in = json_decode($request->getContent(), TRUE, 512);
    $context = is_array($post_in['context'])?$post_in['context']:[];
    $parsed_collection = $this->collector->collect($post_in['event'], $context);

    $saving_status = $this->saveEntity($post_in, $parsed_collection);

    // Send back the bearer despite it's not in Specification
      return new JsonResponse($saving_status + [
        'method' => 'GET',
      ],200 ,[ 'Authorization' => 'Bearer ' . $jwtToken ]);
  }

  /**
   * Save the entity in drupal
   *
   * @param Array $message
   * @param Array $collection
   *
   * @return Array
   *   With the Status and the url if succesful
   */
  protected function saveEntity($message, $collection){
     $hook_settings = $this->configFactory->get('matrix_webhook.hooks_settings');
    $config_mapping = $this->configFactory->get('matrix_webhook.field_mapping');
     $data = ['text' => 'Nothing collected.'];
     $author = strlen($collection['replied_author']) > 2 ?$collection['replied_author']:$message['sender'];
     $collection['text'] .= $message['context']; 
     error_log(print_r($collection, true));
     // TODO !!! PLACE THE USE OF CONTEXT HERE!!! (group_id, sunny day, or whatever)
     // Link media case
     if ($hook_settings->get('capture_links') && count($collection['links']) > 0){
       $node = $this->entityTypeManager->getStorage('node')->create([
         'type' => $config_mapping->get('capture_links.content_type'),
         'uid' => $this->getUserFromMatrixUserName($author),
         $config_mapping->get('capture_links.fields.link') => $collection['links'],
         $config_mapping->get('capture_links.fields.description') => trim($collection['remain_text']),
         'title' => Unicode::truncate(trim($collection['text'])?:$collection['links'][0], 255, TRUE, TRUE),
       ]);
       if($config_mapping->get('capture_links.fields.vocabulary')){
         $vocabulary_field = $config_mapping->get('capture_links.fields.vocabulary');
         $tids = $this->getOrCreate($node, $vocabulary_field, $collection['hashtags']);
         $node->set($config_mapping->get('capture_links.fields.vocabulary'), $tids);
       }
       $node->save();
       $data['url'] = $node->toUrl('canonical', ["absolute" => TRUE])->toString();
       $data['text'] = t("Link(s) collected in @url by @user", ["@url" => $data['url'], "@user" => $author]);
     }
     return $data;
  }

/**
   * Get user name from specified matrix alias field, or the defined user if
   * not found.
   *
   * @param string $matrix_username
   *   The matrix username
   *
   * @return User
   *   The drupal user id of user found with the username defined in specified
   *   field, the fallback specified bot user or finally the user with uid 1.
   */
  protected function getUserFromMatrixUserName($matrix_username){
    $config_mapping = $this->configFactory->get('matrix_webhook.field_mapping');
    if ($config_mapping->get('matrix_user.username_field')){
      $query = $this->entityQuery
      ->get('user')
      ->condition($config_mapping->get('matrix_user.username_field'), $matrix_username);
      $user_ids = $query->execute();
      if ($user_ids){
        return array_keys($user_ids)[0];
      }
    }
    return $config_mapping->get('matrix_user.bot_user')?:1;
  }

/**
 * Get or create all the taxonomies indicated
 *
 * @param \Drupal\node\NodeInterface $node
 *   The node which vocabulary terms will be stored
 * @param string $vocabulary_field
 *   The name of the field which stores vocabulary terms
 * @param array $terms
 *   An array with terms to be used or created
 *
 * @return array
 *   The ids of the terms used.
 */
protected function getOrCreate(NodeInterface $node, $vocabulary_field, array $terms){
  //get First bundle:
  $target_vocabulary = array_keys($node->getFieldDefinition($vocabulary_field)->getSetting("handler_settings")['target_bundles'])[0];
  // if failing check entity_autocomplete check of target bundles in drupal
  // api.
  $ctids = [];
  foreach($terms as $term){
    $target_terms = taxonomy_term_load_multiple_by_name($term, $target_vocabulary);
    $target_term = current($target_terms);
    $vid = \Drupal::entityManager()->getStorage('taxonomy_term')->loadTree($target_vocabulary);
    if($target_term == NULL) {
       $target_term = Term::create([
         'name' => $term,
         'vid' => taxonomy_vocabulary_get_names()[$target_vocabulary]
       ]);
       $target_term->save();
    }
    $ctids[] = $target_term->id();
  }
  return $ctids;
}

}
