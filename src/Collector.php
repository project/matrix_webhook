<?php

namespace Drupal\matrix_webhook;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Utility\Html;

/**
 * Collector service.
 */
class Collector {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The text that will be modified across filters
   *
   * @var string
   */
  protected $remainText;

  /**
   * An array where extracted links are stored.
   *
   * @var Array
   */
  protected $links;

  /**
   * An array where extracted hashtags are stored.
   *
   * @var Array
   */
  protected $hashtags;

  /**
   * Constructs a Collector object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * Method description.
   *
   * @param Array $message
   *   The matrix event message.
   * @param Array $context
   *   The static context coming from webhook custom command set. Not used by
   *   now
   *
   * @return Array
   *   The text that is sending back to matrix.
   */
  public function collect(Array $message, Array $context) {
    $config_hook = $this->configFactory->get('matrix_webhook.hooks_settings');
    $config_mapping = $this->configFactory->get('matrix_webhook.field_mapping');
    $data['text'] =  t("No media detected.\nUsage:\n". $this->usage . "\n(Note that by now only urls are processed)");

    $this->remainText = $message['decrypted']?$message['decrypted']['content']['body']:$message['content']['body'];
    $this->links = [];
    $this->hashtags = [];
    //$images = []; // @todo implement other entities;

    $this->extractCommand();
    $this->extractLinks();
    $this->extractHashtags();
    $this->extractRepliedAuthor();

    return [
      'replied_author' => $this->repliedAuthor,
      'text' => $this->remainText,
      'links' => $this->links,
      'hashtags' => $this->hashtags
    ];
  }

		public function extractLinks(){
// or https://github.com/misd-service-development/php-linkify
      // from https://api.drupal.org/api/drupal/core%21modules%21filter%21filter.module/function/_filter_url/8.5.x
      $protocols = \Drupal::getContainer()->getParameter('filter_protocols');
			$protocols = implode(':(?://)?|', $protocols) . ':(?://)?';
			$valid_url_path_characters = "[\\p{L}\\p{M}\\p{N}!\\*\\';:=\\+,\\.\$\\/%#\\[\\]\\-_~@&]";

			// Allow URL paths to contain balanced parens
			// 1. Used in Wikipedia URLs like /Primer_(film)
			// 2. Used in IIS sessions like /S(dfd346)/
			$valid_url_balanced_parens = '\\(' . $valid_url_path_characters . '+\\)';

			// Valid end-of-path characters (so /foo. does not gobble the period).
			// 1. Allow =&# for empty URL parameters and other URL-join artifacts
			$valid_url_ending_characters = '[\\p{L}\\p{M}\\p{N}:_+~#=/]|(?:' . $valid_url_balanced_parens . ')';
			$valid_url_query_chars = '[a-zA-Z0-9!?\\*\'@\\(\\);:&=\\+\\$\\/%#\\[\\]\\-_\\.,~|]';
			$valid_url_query_ending_chars = '[a-zA-Z0-9_&=#\\/]';

			// full path
			// and allow @ in a url, but only in the middle. Catch things like http://example.com/@user/
			$valid_url_path = '(?:(?:' . $valid_url_path_characters . '*(?:' . $valid_url_balanced_parens . $valid_url_path_characters . '*)*' . $valid_url_ending_characters . ')|(?:@' . $valid_url_path_characters . '+\\/))';

			// Prepare domain name pattern.
			// The ICANN seems to be on track towards accepting more diverse top level
			// domains, so this pattern has been "future-proofed" to allow for TLDs
			// of length 2-64.
			$domain = '(?:[\\p{L}\\p{M}\\p{N}._+-]+\\.)?[\\p{L}\\p{M}]{2,64}\\b';
			$ip = '(?:[0-9]{1,3}\\.){3}[0-9]{1,3}';
			$auth = '[\\p{L}\\p{M}\\p{N}:%_+*~#?&=.,/;-]+@';
			$trail = '(' . $valid_url_path . '*)?(\\?' . $valid_url_query_chars . '*' . $valid_url_query_ending_chars . ')?';

			// Match absolute URLs.
			$url_pattern = "(?:{$auth})?(?:{$domain}|{$ip})/?(?:{$trail})?";
			$pattern = "`((?:{$protocols})(?:{$url_pattern}))`u";
			$tasks['filterUrlParseFullLinks'] = $pattern;


			// Match email addresses.
			$url_pattern = "[\\p{L}\\p{M}\\p{N}._+-]{1,254}@(?:{$domain})";
			$pattern = "`({$url_pattern})`u";
			$tasks['filterUrlParseEmailLinks'] = $pattern;

			// Match www domains.
			$url_pattern = "www\\.(?:{$domain})/?(?:{$trail})?";
			$pattern = "`({$url_pattern})`u";
      $tasks['filterUrlParsePartialLinks'] = $pattern;

			// Each type of URL needs to be processed separately. The text is joined and
			// re-split after each task, since all injected HTML tags must be correctly
			// protected before the next task.
			foreach ($tasks as $task => $pattern) {
        // Replace logic, as it only contains plain text, in drupal filter it
        // splits into chunks of [ text , tag, text, tag, ....], now is only
        // text so skip overhead.
        $this->remainText = preg_replace_callback($pattern, [$this, $task], $this->remainText);
      }
	}

  public function extractHashtags(){
    $this->remainText = trim(preg_replace_callback(['/ \#\S+/'],[$this,'filterHashtags'], $this->remainText));
  }

  private function extractCommand(){
    $this->remainText = trim(preg_replace(['/^\!\S+/','/\\n\!\S+/'],"", $this->remainText));
  }

  private function extractRepliedAuthor(){
    $this->remainText = trim(preg_replace_callback('/^\> \<(\S+)\>/',[$this,'filterRepliedAuthor'], $this->remainText));
  }
  public function extractImages(){
      //todo
	}

  /**
 * Makes links out of absolute URLs. And clears it from text.
 *
 * Modified version of drupals _filter_url_parse_full_links to
 * feed a links bag and return a text without these urls.
 * Callback for preg_replace_callback() within _filter_url().
 * @param Array $match
 *   a regexp replace match array.
 *
 * @return
 *   text without these urls
 */
  private function filterUrlParseFullLinks($match) {
    // The $i:th parenthesis in the regexp contains the URL.
    $i = 1;
    $match[$i] = Html::decodeEntities($match[$i]);
    $this->links[] = Html::escape($match[$i]);
    return "";
  }

/**
 * Extracts the hashtags
 *
 * @param Array $match
 *   a regexp replace match array.
 *
 * @return
 *   text without these hashtags
 */
  private function filterHashtags($match) {
    $i = 0;
    $match[$i] = Html::decodeEntities($match[$i]);
    $this->hashtags[] = substr(Html::escape($match[$i]),2);
    return "";
  }

/**
 * Extracts the replied author
 *
 * @param Array $match
 *   a regexp replace match array.
 *
 * @return
 *   text without the replied author
 */
  private function filterRepliedAuthor($match) {
    $i = 1;
    $match[$i] = Html::decodeEntities($match[$i]);
    $this->repliedAuthor = Html::escape($match[$i]);
    return "";
  }
}
